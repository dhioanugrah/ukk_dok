@extends('layouts.admin',['title'=>'Kamar'])

@section('content-header')
<h1 class="m-0"><i class="fas fa-bed"></i>Kamar</h1>
@endsection

@section('content')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col">
                <a href="{{route('admin.create')}}" class="btn btn-primary">
                    <i class="fas fa-plus-circle"></i>Tambah
                </a>
            </div>
        </div>
    </div>
    <div class="card-body p-0">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Kamar</th>
                    <th>Harga Kamar</th>
                    <th>Jumlah Kamar</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1;?>
                @foreach ($data as $item)
                <tr>
                    <td>{{$no++}}</td>
                    <td>{{$item->nama_kamar}}</td>
                    <td>Rp.{{$item->harga_kamar}}</td>
                    <td>{{$item->jum_kamar}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-body pb-0">

    </div>
</div>

@endsection