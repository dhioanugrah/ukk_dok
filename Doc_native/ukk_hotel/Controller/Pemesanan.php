<?php
    require_once("../Config/Connect.php");
    require_once("../Models/Pemesanan.php");

    $pemesanan = new Pemesanan();

    switch($_GET["op"]){
        case "insert":
            $pemesanan->insert_pemesanan($_POST["nama_pemesanan"],
            $_POST["email"],
            $_POST["no_hp"],
            $_POST["nama_tamu"],
            $_POST["id_tipekamar"],
            $_POST["check_in"],
            $_POST["check_out"],
            $_POST["jml_kamar"]);
        break;
        case "getData" :
            $data_hasil = $pemesanan->get_data();
            $data = Array();
            foreach($data_hasil as $row){
                $sub_array = array();
                $sub_array[] = $row["id_pemesanan"];
                $sub_array[] = $row["nama_pemesanan"];
                $sub_array[] = $row["email"];
                $sub_array[] = $row["no_hp"];
                $sub_array[] = $row["nama_tamu"];
                $sub_array[] = $row["cat_name"];                
                $sub_array[] = date("d-M-Y", strtotime($row["check_in"]));
                $sub_array[] = date("d-M-Y", strtotime($row["check_out"]));
                $sub_array[] = $row["jml_kamar"];

                if($row["status"] == "1"){
                    $sub_array[] = '<span class="label label-success">Opened</span>';
                }else{
                    $sub_array[] = '<span class="label label-danger">Closed</span>';
                }

                
                $sub_array[] = '<button type="button" onClick="get('.$row["nama_pemesanan"].');" id="'.$row["nama_pemesanan"].'" class="btn btn-info btn-sm" title="Edit Tiket">
                    <div>
                        <i class="fa fa-edit"></i>
                    </div>
                </button>';
                $data[] = $sub_array;
            }
            $result = array(
                "sEcho" => 1,
                "iTotalRecords" =>count($data),
                "iTotalDisplayRecords" => count($data),
                "aaData" => $data
            );

            echo json_encode($result);
        break;
    }
?>