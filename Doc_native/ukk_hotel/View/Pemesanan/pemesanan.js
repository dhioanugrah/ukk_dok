function init(){
    $("#ticket-form").on("submit", function(e){
        saveAndEdit(e);
    })
}

$(document).ready(function () {
    $.post("../../Controller/Category.php?op=combo", function (data, status) {
        $('#cat_name').html(data);
    })

});

function saveAndEdit(e){
    e.preventDefault();
    var formData = new FormData($("#ticket-form")[0]);

    //Validate the form before insert
    if ($('#nama_pemesanan').val() == '' || $('#no_hp').val() == '') {
        swal("Perhatian", "Silahkan Isi Form Terlebih Dahulu", "warning");
    }else {
        $.ajax({
            url: "../../Controller/Pemesanan.php?op=insert",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function (data) {
                $('#nama_pemesanan').val('');
                $('#no_hp').val('');
                swal("Selamat", "Kamar Sudah Berhasil di Pesan", "success");
            }
        })
    }
}

init();