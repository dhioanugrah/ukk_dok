<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Hotel Baru</title>
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../../Public/plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../Public/css/adminlte.min.css">
  <link rel="stylesheet" href="../../Public/css/style.css">
  
</head>
<body class="hold-transition">
<!-- As a link -->
<nav class="navbar navbar-expand-md navbar-dark bg-info shadow">
    <div class="container">
        <a class="navbar-brand h1" href="index.html">
            <img src="../../Public/images/logo.jpg" width="30" height="30" class="d-inline-block align-top img-circle" alt="Logo">
            Hotel Baru
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="../../index.php">Home</a>
            </li>
        </ul>
        </div>
    </div>
</nav>

<div class="container-fluid p-0">
    <img src="../../Public/images/banner.jpg" class="img img-fluid w-100">
</div>
    <!-- Horizontal Form -->
  <div class="box box-info-center">
        <div class="box-header with-border center">
            <h3 class="box-title">Form Pemesanan</h3>
        </div>
          <!-- /.box-header -->
          <!-- form start -->
        <form method="POST" id="ticket-form" class="form-horizontal">
          <div class="box-body">
                <!-- Nama Pemesan -->
                <div class="form-group">
                  <label for="nama_pemesanan" class="col-sm-2 control-label">Nama Pemesan</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="nama_pemesanan" id="nama_pemesanan" placeholder="Nama Pemesan">
                  </div>
                </div>
                <!-- Email -->
                <div class="form-group">
                  <label for="email" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-5">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                  </div>
                </div>
                <!-- No HP -->
                <div class="form-group">
                  <label for="no_hp" class="col-sm-2 control-label">No Handphone</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" id="no_hp" name="no_hp" placeholder="No Handphone">
                  </div>
                </div>
                <!-- Nama Tamu -->
                <div class="form-group">
                  <label for="nama_tamu" class="col-sm-2 control-label">Nama Tamu</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="nama_tamu" id="nama_tamu" placeholder="Nama Tamu">
                  </div>
                </div>
                <!-- Tipe Kamar -->
                <div class="form-group col-sm-5">
                  <label>Tipe Kamar</label>
                  <select id="cat_name" name="id_tipekamar" class="form-control">
                  </select>
                </div>
                <!-- Check IN dan Check Out -->
                <div class="form-group col-sm-5">
                  <label>Check-In:</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="date" name="check_in" class="form-control pull-left" id="check-in">
                  </div>
                    <label>Check-Out:</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="date" name="check_out" class="form-control pull-right" id="check-out">
                  </div>
                </div>
                <!-- Jumlah Kamar -->
                <div class="form-group col-sm-8">
                  <label for="jml_kamar" class="col-sm-2 control-label">Jumlah Kamar</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="jml_kamar" id="jml_kamar" placeholder="Jumlah Kamar">
                  </div>
                </div>
            
                <!-- /.input group -->
              </div>
            </div>
            <!-- /.box-body -->
              <div class="box-footer">
                <button type="reset" class="btn btn-default">Cancel</button>
                <button type="submit" name="action-save" value="save" class="btn btn-info pull-right">Pesan Sekarang</button>
              </div>
            <!-- /.box-footer -->
          </div>
        </form>
  </div>
    <!-- /.box -->

<!-- jQuery -->
<script src="../../Public/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../Public/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../Public/js/adminlte.min.js"></script>
<!-- daterange picker -->
<link rel="stylesheet" href="../../Public/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<!-- bootstrap datepicker -->
<script src="../../Public/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- date-range-picker -->
<script src="../../Public/bower_components/moment/min/moment.min.js"></script>
<script src="../../Public/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- sweet alert -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="pemesanan.js"></script>
<!-- Page script -->
<!-- <script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, locale: { format: 'MM/DD/YYYY hh:mm A' }})
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script> -->
</body>
</html>
