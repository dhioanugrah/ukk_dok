
<!-- Login Sebagai Role ID 1 = ADMIN -->
<?php
  if($_SESSION["role_id"]==1){
?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        </div>
        <div class="pull-left info">
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="../DetailKamar/">
            <i class="fa fa-bed"></i> <span>Data Kamar & Fasilitas Hotel</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        <li>
          <a href="../DataPemesanan/">
            <i class="fa fa-ticket"></i> <span>Data Pemesanan</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>        
      </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- Login Sebagai Role ID 2 = Resepsionis -->
<?php
    }else{
?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        </div>
        <div class="pull-left info">
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="../DataPemesanan/">
            <i class="fa fa-ticket"></i> <span>Data Pemesanan</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>       
      </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<?php
    }
?>