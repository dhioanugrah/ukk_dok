<?php
  require_once("../../Config/Connect.php");
  if(isset($_SESSION["id_user"])){
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title>AdminLTE 2 | Blank Page</title>
  <!-- Link -->
  <?php require_once("../LayoutPartial/link.php"); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <!-- Header -->
    <?php require_once("../LayoutPartial/header.php"); ?>
    <!-- nav -->
    <?php require_once("../LayoutPartial/nav.php"); ?>


    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Pemesanan
            <small>DataPemesanan</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="../Home/index.php"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#">DataPemesanan</a></li>
        </ol>
    </section>

<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box">
            <div class="box-header">
              <h3 class="box-title">DataPemesanan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="ticket-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th style="width: 7%;">No. Pemesan</th>
                  <th>Nama Pemesanan</th>
                  <th>Email</th>
                  <th>No HP</th>
                  <th>Nama Tamu</th>
                  <th>Tipe Kamar</th>
                  <th>Check IN</th>
                  <th>Check Out</th>
                  <th>Jumlah Kamar</th>
                  <th style="width: 7%;">Status Pemesan</th>
                  <th>Pilihan</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
        </div>
    <!-- /.box -->
</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- script -->
<?php require_once("../LayoutPartial/script.php"); ?>
<script src="dataPemesanan.js" type="text/javascript"></script>
</body>
</html>
<?php
  }else{
    header("Location: ".Connect::base_url()."View/DataPemesanan/index.php");
  }
?>