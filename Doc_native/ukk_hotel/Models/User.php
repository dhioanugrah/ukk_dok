<?php
    class User extends Connect{

        public function login(){
            $connect = parent::connection();
            parent::set_name();

            if(isset($_POST["submit"])){
                $nick = $_POST["user_nickname"];
                $pass = $_POST["user_pass"];
                $role = $_POST["role_id"];

                if(empty($nick) and empty($pass)){
                    header("location:".connect::base_url()."View/LoginAdmin/index.php?m=2");
                    exit();
                }else{
                    $sql = "SELECT * FROM tb_user WHERE user_nickname=? and user_pass=? and role_id=?and status=1";
                    $stmt = $connect->prepare($sql);
                    $stmt->bindValue(1, $nick);
                    $stmt->bindValue(2, $pass);
                    $stmt->bindValue(3, $role);
                    $stmt->execute();
                    $result = $stmt->fetch();

                    if(is_array($result) and count($result)>0){
                        $_SESSION["id_user"]=$result["id_user"];
                        $_SESSION["user_nickname"]=$result["user_nickname"];
                        $_SESSION["role_id"]=$result["role_id"];
                        header("Location:".connect::base_url()."View/Home/");
                        exit();
                    }else{
                        header("Location:".connect::base_url()."View/LoginAdmin/index.php?m=1");
                        exit();
                    }
                }
            }
        }
    }
?>